return {
    name = "the-sparrows-nest",
    title = "麻雀窝",
    author = "william-wordsworth",
    subtitle = "The Sparrow's Nest",
    date = "2016-09-22",
    teaser = [[
> Behold, within the leafy shade,

> Those bright blue eggs together laid!

> On me the chance-discovered sight

> Gleamed like a vision of delight.

]],
    preclude = [[
作者：[威廉·华兹华斯](william-wordsworth.html)（William Wordsworth）]

译者：杨德豫

为诗人和诗人的妹妹多萝西作传的人们曾指出：多萝西对自然景物和人事的感受比诗人更敏锐，观察更细致，体会也更细微；有时是在她的启发和引导下，诗人才加深了对事物的理解和领悟；一些优秀诗篇的写成，也是首先由她触发了诗人的灵感。在这首诗中，诗人说他的妹妹给她以眼、耳、心等等，就是指多萝西使他目明耳聪，心灵开窍。
]],
    english = [[

> Behold, within the leafy shade,

> Those bright blue eggs together laid!

> On me the chance-discovered sight

> Gleamed like a vision of delight.

> I started--seeming to espy

> 　　The home and sheltered bed,

> The Sparrow's dwelling, which, hard by

> My Father's house, in wet or dry

> My sister Emmeline and I

> 　　Together visited.

> 　

> She looked at it and seemed to fear it;

> Dreading, tho' wishing, to be near it;

> Such heart was in her, being then

> A little Prattler among men.

> The Blessing of my latter years

> 　　 Was with me when a boy:

> 　

> She gave me eyes, she gave me ears;

> And humble cares, and delicate fears;

> A heart, the fountain of sweet tears;

> 　　And love, and thought, and joy.

]],
    chinese = [[

> 快瞧，这绿叶浓阴里面，

> 藏着一窝青青的鸟蛋！

> 这偶然瞥见的景象，看起来

> 像迷人的幻境，闪烁着光彩。

> 我惊恐不安——仿佛在窥视

> 　　别人隐秘的眠床；

> 这个窝靠近我们的居室，

> 不分晴雨，也不问干湿，

> 我和艾米兰妹妹总是

> 　　一道去把它探望。

> 　

> 她望着鸟窝，好像有点怕：

> 又想挨近它，又怕惊动它；

> 她还是口齿不清的小姑娘，

> 便有了这样一副好心肠！

> 我后来的福分，早在童年

> 　　便已经与我同在：

> 　

> 她给我一双耳朵，一双眼睛，

> 锐敏的忧惧，琐细的挂牵，

> 一颗心——甜蜜泪水的泉源，

> 　　思想，快乐，还有爱。

]],
    thought = [[
这首诗给我的感触是，诗人和她妹妹间的美好关系。

一起玩耍，一起安静地观察鸟窝。

更难能可贵的是，两人都有一颗热爱自然的心，既是兄妹，更是知己。

妹妹对鸟窝，又想靠近，又怕惊动鸟儿，细腻委婉，天真无邪，善良动人。

而诗人感谢妹妹的陪伴，感谢妹妹给了他灵感。

我想起我的姐姐，当年是不是也那么美好，可是当年的我并不珍惜。

如今想来，念念不忘。
]],
}
