return {
    name = "my-heart-leaps-up-when-i-behold-a-rainbow",
    title = "我一见彩虹，心儿便跳荡不止",
    author = "william-wordsworth",
    subtitle = "",
    date = "2016-09-21",
    teaser = [[

> My heart leaps up when I behold

> 　　A rainbow in the sky:

> So was it when my life began;

> So is it now I am a man;

> So be it when I shall grow old,

> 　　Or let me die!

]],
    preclude = [[
作者：[威廉·华兹华斯](william-wordsworth.html)（William Wordsworth）]

译者：杨德豫
]],
    english = [[

> My heart leaps up when I behold

> 　　A rainbow in the sky:

> So was it when my life began;

> So is it now I am a man;

> So be it when I shall grow old,

> 　　Or let me die!

> The child is father of the Man;

> And I could wish my days to be

> Bound each to each by natural piety.

]],
    chinese = [[

> 我一见彩虹高悬天上，

> 　　心儿便跳荡不已：

> 从前小时候就是这样；

> 如今长大了还是这样；

> 以后我老了也要这样；

> 　　否则，不如死！

> 儿童乃是成人的父亲；

> 我可以指望：我一世光阴

> 自始自终贯穿着对自然的虔诚。

]],
    thought = [[

这首诗，喜欢的是诗人一见彩虹的喜悦，犹如初恋，对大自然美景的喜爱，自小维持至今，也愿持续到老来之时，心灵仿佛受到了触动。

]],
}
