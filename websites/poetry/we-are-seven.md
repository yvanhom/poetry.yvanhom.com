return {
    name = "we-are-seven",
    title = "我们是七个",
    author = "william-wordsworth",
    subtitle = "We are seven",
    date = "2016-09-24",
    teaser = [[
> --A simple child,

> 　That lightly draws its breath,

> And feels its life in every limb,

> 　What should it know of death?

]],
    preclude = [[
作者：[威廉·华兹华斯](william-wordsworth.html)（William Wordsworth）]

译者：杨德豫

这首诗深得德·昆西的喜爱，德·昆西因为在这首诗找到共鸣，而成为华兹华斯的超级粉丝，并写了一封粉丝信给华兹华斯，企求华兹华斯做他的朋友。

]],
    english = [[

> --A simple child,

> 　That lightly draws its breath,

> And feels its life in every limb,

> 　What should it know of death?

> 　

> I met a little cottage Girl:

> 　 She was eight years old, she said;

> Her hair was thick with many a curl

> 　That clustered round her head.

> 　

> She had a rustic, woodland air,

> 　And she was wildly clad:

> Her eyes were fair, and very fair;

> 　--Her beauty made me glad.

> 　

> "Sisters and brothers, little Maid,

> 　How many may you be?"

> "How many? Seven in all," she said,

> 　And wondering looked at me.

> 　

> "And where are they? I pray you tell."

> 　She answered, "Seven are we;

> And two of us at Conway dwell,

> 　And two are gone to sea.

> 　

> "Two of us in the church-yard lie,

> 　My sister and my brother;

> And, in the church-yard cottage, I

> 　Dwell near them with my mother."

> 　

> "You say that two at Conway dwell,

> 　And two are gone to sea,

> Yet ye are seven! I pray you tell,

> 　Sweet Maid, how this may be."

> 　

> Then did the little Maid reply,

> 　"Seven boys and girls are we;

> Two of us in the church-yard lie,

> 　Beneath the church-yard tree."

> 　

> "You run about, my little Maid,

> 　Your limbs they are alive;

> If two are in the church-yard laid,

> 　Then ye are only five."

> 　

> "Their graves are green, they may be seen,"

> 　The little Maid replied,

> "Twelve steps or more from my mother's door,

> 　And they are side by side.

> 　

> "My stockings there I often knit,

> 　My kerchief there I hem;

> And there upon the ground I sit,

> 　And sing a song to them.

> 　

> "And often after sun-set, Sir,

> 　When it is light and fair,

> I take my little porringer,

> 　And eat my supper there.

> 　

> "The first that died was sister Jane;

> 　In bed she moaning lay,

> Till God released her of her pain;

> 　And then she went away.

> 　

> "So in the church-yard she wad laid;

> 　And, when the grass was dry,

> Together round her grave we played,

> 　My brother Jone and I.

> 　

> "And when the ground was white with snow,

> 　And I could run and slide,

> My brother John was forced to go,

> 　And he lied by her side."

> 　

> "How many are you, then," said I,

> 　"If they two are in heaven?"

> Quick was the little Maid's reply,

> 　"O Master! We are seven."

> 　

> "But they are dead; those two are dead!

> 　Their spirits are in heaven!"

> 'Twas throwing words away; for still

> The little Maid would have her will,

> 　And said, "Nay, we are seven!"

]],
    chinese = [[

> ——天真的孩子，

> 　　呼吸得那样柔和！

> 只感到生命充沛在四肢，

> 　　对死亡，她懂得什么？

> 　

> 我碰到一个乡下小姑娘，

> 　　她说，她今年八岁；

> 鬈曲的头发盘绕在头上，

> 　　密密丛丛的一堆。

> 　

> 她一身山林乡野气息，

> 　　胡乱穿几件衣衫；

> 眼睛挺秀气，十分秀气，

> 　　——那模样叫我喜欢。

> 　

> “你兄弟姐妹一共有几个？

> 　　说给我听听，小姑娘！”

> “几个？一共是七个，”她说，

> 　　惊奇地向我张望。

> 　

> “他们在哪里？说给我听听。”

> 　　她说：“我们是七个；

> 两个当水手，在海上航行，

> 　　两个在康韦住着。

> 　

> “还有两个躺进了坟地——

> 　　我姐姐和我哥哥；

> 靠近他们，教堂边，小屋里，

> 　　住着我妈妈和我。”

> 　

> “你说有两个在康韦住着，

> 　　有两个到了海上，

> 却又说你们还有七个！

> 　　是怎么算的，好姑娘？”

> 　

> 这位姑娘随口回答：

> 　　“我们七兄弟姐妹，

> 有两个睡在那棵树底下——

> 　　那儿是教堂的坟堆。”

> 　

> “你到处跑来跑去，小姑娘，

> 　　你手脚那么活泼；

> 既然坟堆里睡下了一双，

> 　　那你们还剩下五个。”

> 　

> “坟堆看得见，青绿一片，”

> 　　这位小姑娘答道，

> “离我家门口十二步左右，

> 　　两座坟相挨相靠。

> 　

> “那儿，我常常织我的毛袜，

> 　　把手绢四边缝好；

> 我常常靠近坟头坐下，

> 　　给他们唱一首小调。

> 　

> “先生，只要碰上了好天气，

> 　　太阳下了山，还不暗，

> 我就把我的小粥碗端起，

> 　　上那儿吃我的晚饭。

> 　

> “我姐姐珍妮先走一步；

> 　　她躺着，哼哼叫叫，

> 上帝解除了她的痛苦，

> 　　她便悄悄地走掉。

> 　

> “她被安顿在坟地里睡下；

> 　　等她的墓草一干，

> 我们便在她坟边玩耍——

> 　　我和我哥哥约翰。

> 　

> “等到下了雪，地下一片白，

> 　　我可以乱跑乱滑，

> 我哥哥约翰却又离开，

> 　　在我姐姐身边躺下。”

> 　

> “有两个进了天国，”我说

> 　　“那你们还剩几个？”

> 小姑娘回答得又快又利索：

> 　　“先生！我们是七个。”

> 　

> “可他们死啦，那两个死啦！

> 　　他们的灵魂在天国！”

> 这些话说了也是白搭，

> 小姑娘还是坚持回答：

> 　　“不，我们是七个！”

]],
    thought = [[

这首诗其实我并不喜欢，就因为诗中的那位 Master，他究竟想干什么，不断地想说服小姑娘，他们是五个，就像是冰冷冷的机器，我难以把自己代入诗中的“我”。

不过诗中女孩的坚持，对死去姐姐哥哥的怀念，还时常到他们坟前陪着他们，让我动容。真是纯真、可爱、善良。有此家人，有此种思念，虽然死了，但我觉得他们也是欣慰的。

灵魂与妹妹一起玩耍。

而妹妹的孤独，也从诗中透露出来，兄弟姐妹本来七个，结果各奔东西了四个，走了两个，只剩下她和妈妈，想来就悲伤。

]],
}
