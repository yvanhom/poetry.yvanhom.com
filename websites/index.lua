return {
    ["sitetitle"] = "英诗漫游",
    ["sitesubtitle"] = "我独自游荡，像一朵孤云",
    ["content"] = [[

近来喜欢浪漫主义诗歌，于是便不负责任地弄个网站，写点东西，自娱自乐。

    ]],
    
    ["lists"] = {
        {
            id = "poets",
            class = "two",
            desc = "诗人荟萃",
            icon = "fa-user",
            max = 5,
        },
        {
            id = "poetry",
            class = "four",
            desc = "诗歌荟萃",
            icon = "fa-th",
            max = 5,
        }
    }
}
