all:
	lua src/yvgensite.lua websites/ templates/ build https://poetry.yvanhom.com

clean:
	rm -rf build/*.html build/*.xml
