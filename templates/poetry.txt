<div id="main">
    <section id="top" class="one dark cover">
		<div class="container">
			<header>
			    <h2>{{title}}</h2>
                <h3>{{subtitle}}</h3>
			    <p>{{date}}</p>
			</header>
			{{preclude}}
	    </div>
	</section>
    <section id="english" class="two">
		<div class="container">
			<header>
			    <h2>英文</h2>
			</header>
			{{english}}
	    </div>
	</section>
    <section id="chinese" class="three">
		<div class="container">
			<header>
			    <h2>中文</h2>
			</header>
			{{chinese}}
	    </div>
	</section>
    <section id="thought" class="four">
		<div class="container">
			<header>
			    <h2>感想</h2>
			</header>
			{{thought}}
	    </div>
	</section>
</div>
