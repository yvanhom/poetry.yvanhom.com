return function(area)
    local text = {
        "\n",
    	[[<nav id="nav">]],
        "    <ul>"
    }
    
    local id
    for i, v in ipairs(area) do
        if string.sub(v.value, 1, 1) == "#" then
            id = string.sub(v.value, 2) .. "-link"
        end
        table.insert(text, string.format([[        <li><a href="%s" class="skel-layers-ignoreHref" id = "%s"><span class="icon %s">%s</span></a></li>]], v.value or "#", id, v.icon or "fa-th", v.key))
    end
    
    table.insert(text, "    </ul>")
    table.insert(text, "</nav>\n")
    return table.concat(text, "\n")
end
