local md = require "markdown"

local sectiontext = [[
    <section id="{{id}}" class="{{class}}">
		<div class="container">
			<header>
			    <h2>{{desc}}</h2>
			</header>
			<div class="row">
			    {{items}}
			</div>
	    </div>
	</section>
]]

local itemtext = [[
    <div class="6u 12u$(mobile)">
        <a href="{{link}}">
        <article class="item">
            <!--a href="{{link}}" class="image fit"><img src="{{image}}" /></a-->
            <header>
                <h3>{{title}}</h3>
            </header>
            {{content}}
        </article>
        </a>
    </div>
]]

local function item(id, list, max)
    local t = {}
    local text = ""
    local post = "\n<p>[显示全部...]</p>"
    for i, it in ipairs(list) do
        t.link = it.name .. ".html"
        t.image = it.image or ""
        t.title = it.title
        t.content = md(it.teaser) .. post
        text = text .. string.gsub(itemtext, "{{(%w+)}}", t)
        if max and i > max then
            break
        end
    end
    
    -- Add more
    if max then
        t.link = id .. ".html"
        t.image = ""
        t.title = "..."
        t.content = post
        text = text .. string.gsub(itemtext, "{{(%w+)}}", t)
    end
    
    return text
end

return function(lists, more)
    local text = ""
    local t = {}
    for i, li in ipairs(lists) do
        t.id = li.id
        t.class = li.class
        t.desc = li.desc
        t.items = item(li.id, li, more and li.max)
        text = text .. string.gsub(sectiontext, "{{(%w+)}}", t)
    end
    
    return text
end
