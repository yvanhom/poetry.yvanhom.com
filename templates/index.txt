<div id="main">
    <section id="top" class="one dark cover">
		<div class="container">
			<header>
			    <h2>{{title}}</h2>
                <h3>{{subtitle}}</h3>
			    <p>{{pagedate}}</p>
			</header>
			{{content}}
	    </div>
	</section>
    {{sections}}
</div>
