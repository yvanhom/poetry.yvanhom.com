require "luarocks.loader"

local plfile = require "pl.file"
local pldir = require "pl.dir"
local md = require "markdown"

local function loadTemplate(dir, web)
    local t = {}
    local header = plfile.read(dir .. "/header.txt")
    local footer = plfile.read(dir .. "/footer.txt")
    local index = plfile.read(dir .. "/index.txt")
    local list = plfile.read(dir .. "/list.txt")
    t.sitebar = dofile(dir .. "/sitebar.lua")
    t.section = dofile(dir .. "/section.lua")
    
    t.allindex = header ..  index .. footer
    t.alllist = header .. list .. footer
    
    for i, list in ipairs(web.lists) do
        local l = plfile.read(dir .. "/" .. list.id .. ".txt")
        t[list.id] = header .. l .. footer
    end
    
    return t
end

local function loadList(dir, list)
    local files = pldir.getfiles(dir, "*.md")
    for i, f in ipairs(files) do
        list[i] = dofile(f)
    end
end

local function loadWebsite(dir)
    local web = dofile(dir .. "/index.lua")
    
    for i, list in pairs(web.lists) do
        loadList(dir .. "/" .. list.id, list)
    end
    
    -- Sort poetry
    table.sort(web.lists[2], function(a, b)
        if a.date and b.date then
            return a.date > b.date
        elseif a.date then
            return true
        else
            return false
        end
    end)
    web.sitebar = dofile(dir .. "/sidebar.lua")
    
    return web
end

local function genIndex(template, web, todir)
    local t = {}
    t["pagetitle"] = web.sitetitle .. " - " .. web.sitesubtitle
    
    local nav = {}
    local home_nav = {
        key = "主页",
        value = "#top",
        icon = "fa-home",
    }
    table.insert(nav, home_nav)
    for i, list in ipairs(web.lists) do
        local item = {}
        item.key = list.desc
        item.value = "#" .. list.id
        item.icon = list.icon
        table.insert(nav, item)
    end
    
    for i, item in ipairs(web.sitebar) do
        table.insert(nav, item)
    end
    
    t["navigation"] = template.sitebar(nav)
    
    t["sitetitle"] = web.sitetitle
    t["sitesubtitle"] = web.sitesubtitle
    t["title"] = web.sitetitle
    t["subtitle"] = web.sitesubtitle
    t["pagedate"] = ""
    t["content"] = md(web.content)
    
    t["sections"] = template.section(web.lists, true)
    
    local text = string.gsub(template.allindex, "{{(%w+)}}", t)
    
    plfile.write(todir .. "/index.html", text)
    
    web.sitemap = {}
    table.insert(web.sitemap, { name = "index", date = "2016-09-22" })
end

local function genListPage(template, web, list, todir)
    
    local t = {}
    t["pagetitle"] = list.id .. " | " .. web.sitetitle
    t["navigation"] = web.navigation
    t["sitetitle"] = web.sitetitle
    t["sitesubtitle"] = web.sitesubtitle
    t["title"] = list.desc
    t["subtitle"] = ""
    t["pagedate"] = ""
    t["content"] = ""
    
    local lists = { list }
    
    t["sections"] = template.section(lists, false)
    
    local text = string.gsub(template.alllist, "{{(%w+)}}", t)
    
    plfile.write(todir .. "/" .. list.id .. ".html", text)
    
    table.insert(web.sitemap, { name = list.id, date = "2016-09-22" })
end

local function genPoetPage(alltemp, template, web, page, todir)
    local t = {}
    
    t["pagetitle"] = page.title .. " | " .. web.sitetitle
    t["navigation"] = web.navigation
    t["sitetitle"] = web.sitetitle
    t["sitesubtitle"] = web.sitesubtitle

    t["title"] = page.title
    t["subtitle"] = page.subtitle or ""
    t["content"] = md(page.content)
    
    t["poetry"] = alltemp.section({page.poetry})
    
    local text = string.gsub(template, "{{(%w+)}}", t)
    
    plfile.write(todir .. "/" .. page.name .. ".html", text)
    
    table.insert(web.sitemap, { name = page.name, date = "2016-09-22" })
end

local function genPoetryPage(alltemp, template, web, page, todir)
    local t = {}
    
    t["pagetitle"] = page.title .. " | " .. web.sitetitle
    t["navigation"] = web.navigation
    t["sitetitle"] = web.sitetitle
    t["sitesubtitle"] = web.sitesubtitle

    t["title"] = page.title
    t["subtitle"] = page.subtitle or ""
    t["date"] = page.date
    t["preclude"] = md(page.preclude)
    t["english"] = md(page.english)
    t["chinese"] = md(page.chinese)
    t["thought"] = md(page.thought)
    
    local text = string.gsub(template, "{{(%w+)}}", t)
    
    plfile.write(todir .. "/" .. page.name .. ".html", text)
    table.insert(web.sitemap, { name = page.name, date = page.date })
end

local function classifyPoetryByPoet(web)
    local poets = web.lists[1]
    local poetry = web.lists[2]
    
    for i, p in ipairs(poets) do
        p.poetry = {
            id = "poetry",
            class = "two",
            desc = "诗人笔下的诗词",
        }
        for j, p2 in ipairs(poetry) do
            if p2.author == p.name then
                table.insert(p.poetry, p2)
            end
        end
    end
end

local function genPages(template, web, todir)
    local nav = {}
    local home_nav = {
        key = "主页",
        value = "index.html",
        icon = "fa-home",
    }
    table.insert(nav, home_nav)
    for i, list in ipairs(web.lists) do
        local item = {}
        item.key = list.desc
        item.value = list.id .. ".html"
        item.icon = list.icon
        table.insert(nav, item)
    end
    for i, item in ipairs(web.sitebar) do
        table.insert(nav, item)
    end
    web.navigation = template.sitebar(nav)
    
    classifyPoetryByPoet(web)
    for i, list in ipairs(web.lists) do
        genListPage(template, web, list, todir)
        if list.id == "poetry" then
            for j, page  in ipairs(list) do
                genPoetryPage(template, template[list.id], web, page, todir)
            end
        else
            for j, page in ipairs(list) do
                genPoetPage(template, template[list.id], web, page, todir)
            end
        end
    end
end

local function genSiteMap(web, todir, url)
    local smtemp = [[<?xml version="1.0"?>
    <urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
    {{pagelist}}
    </urlset>
    ]]

    local pagetemp = [[
    <url>
        <loc>{{site}}/{{name}}.html</loc>
        <lastmod>{{date}}T00:00:00+00:00</lastmod>
    </url>
    ]]
    
    local text = ""
    for i, page in pairs(web.sitemap) do
        page.site = url
        text = text .. string.gsub(pagetemp, "{{(%w+)}}", page)
    end
    local t = { pagelist = text }
    text = string.gsub(smtemp, "{{(%w+)}}", t)
    
    plfile.write(todir .. "/sitemap.xml", text)
end

local function generate(template, web, todir, url)
    genIndex(template, web, todir)
    genPages(template, web, todir)
    genSiteMap(web, todir, todir)
end

local function run(fromdir, templatedir, todir, url)
    local website = loadWebsite(fromdir)
    local template = loadTemplate(templatedir, website)
    
    generate(template, website, todir)
    genSiteMap(website, todir, url)
end

if #arg ~= 4 then
    print("lua yvgensite.lua sourceDir templateDir targetDir url")
    os.exit()
end

run(arg[1], arg[2], arg[3], arg[4])
